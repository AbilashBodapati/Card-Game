

/**
 * TestCardDeck
 *
 * A simple test framework used to demonstrate the design of Java classes.
 *
 * @author Jeremy Morris
 * @Subauthor Abilash Bodapati
 * @version 20190211
 */
public class Test_CardDeck {

    public static void display(CardDeck myDeck) {
        System.out.println("Deck contains:" + myDeck);
        int count = 0;
        while (myDeck.remainingCards() > 0) {
            PlayingCard c1 = myDeck.dealNext();
            System.out.println("Card #" + count + " is " + c1);
            count = count + 1;
        }
        System.out.println("All cards displayed");

        System.out.println("Deck contains: " + myDeck);
    }

    public static void shuffledDisplay(CardDeck myDeck) {

        myDeck.shuffle();
        display(myDeck);

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        CardDeck myDeck = new CardDeck();
        myDeck.newDeck();

        display(myDeck);

        System.out.println("\n----------Shuffling Time--------------");

        // Loop 5 times to check if the shuffle function works.
        for (int i = 1; i <= 5; i++) {
            // Add all the cards back into the deck and shuffle them
            myDeck.newDeck();
            System.out.println("\n" + "Shuffle number " + i + ":- ");
            shuffledDisplay(myDeck);
            System.out.println(
                    "----------END OF SHUFFLE NUMBER " + i + "------------");
        }
    }

}