
/**
 * CardGameTest
 *
 *   A simple test framework used to demonstrate the design
 *   of Java classes.
 *
 *   @author Jeremy Morris
 *   @Subauthor Abilash Bodapati
 *   @version 20190211
 */
import java.util.ArrayList;
import java.util.List;

public class CardGameTest {

    public static void main(String[] args) {
        CardDeck myDeck = new CardDeck();
        myDeck.newDeck();
        myDeck.shuffle();

        List<PlayingCard> myHand = deal(myDeck, 5);
        List<PlayingCard> yourHand = deal(myDeck, 5);

        boolean victory = evaluate(myHand, yourHand);

        if (victory) {
            System.out.println("I win!");
            System.out.println("My hand: " + myHand);
            System.out.println("Your hand: " + yourHand);
        } else {
            System.out.println("You win!");
            System.out.println("My hand: " + myHand);
            System.out.println("Your hand: " + yourHand);
        }
    }

    public static List<PlayingCard> deal(CardDeck deck, int toDeal) {
        ArrayList<PlayingCard> finalHand = new ArrayList<PlayingCard>();

        for (int i = 0; i < toDeal; i++) {
            PlayingCard card = deck.dealNext();
            finalHand.add(card);
        }

        return finalHand;
    }

    public static boolean evaluate(List<PlayingCard> hand1,
            List<PlayingCard> hand2) {
        boolean h1Victor = true;
        // TODO - implement this so that the code returns a value
        //  of true if one of the cards in the list hand1 is the
        //  highest card between the two hands, false otherwise.

        for (int i = 0; i < hand1.size(); i++) {
            for (int j = 0; j < hand2.size(); j++) {
                if (hand1.get(i).compareTo(hand2.get(j)) == 1) {
                    h1Victor = true;

                } else {
                    h1Victor = false;
                }
            }
        }

        return h1Victor;
    }
}
