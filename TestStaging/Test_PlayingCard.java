

/**
 * TestPlayingCard
 *
 * A simple test framework used to demonstrate the design of Java classes.
 *
 * @author Jeremy Morris
 * @subauthor Abilash Bodapati
 * @version 20190211
 */

public class Test_PlayingCard {

    public static void displayCard(PlayingCard c1, String truth) {
        System.out.println("Should be: " + truth);
        System.out.println("Suit is: " + c1.getSuit());
        System.out.println("Value is: " + c1.getValue());
        System.out.println("String value of card is: " + c1);
        System.out.println();
    }

    public static void equality(PlayingCard c1, PlayingCard c2) {
        if (c1.equals(c2)) {
            System.out.println("" + c1.getValue() + "" + c1.getSuit()
                    + " is equal to " + c2.getValue() + c2.getSuit());
        } else {
            System.out.println("" + c1.getValue() + "" + c1.getSuit()
                    + " is not equal to " + c2.getValue() + c2.getSuit());
        }
    }

    public static void comparison(PlayingCard c1, PlayingCard c2) {
        if (c1.compareTo(c2) == -1) {
            System.out.println("" + c1.getValue() + "" + c1.getSuit()
                    + " is less than " + c2.getValue() + c2.getSuit());

        } else if (c1.compareTo(c2) == 1) {
            System.out.println("" + c1.getValue() + "" + c1.getSuit()
                    + " is greater than " + c2.getValue() + c2.getSuit());

        } else if (c1.compareTo(c2) == 0) {
            System.out.println("" + c1.getValue() + "" + c1.getSuit()
                    + " is equal to " + c2.getValue() + c2.getSuit());
        }
    }

    public static void main(String[] args) {
        PlayingCard c1 = new PlayingCard('S', 'A');
        displayCard(c1, "Ace of Spades");

        PlayingCard c2 = new PlayingCard('D', '3');
        displayCard(c2, "3 of Diamonds");

        PlayingCard c3 = new PlayingCard('H', 'Q');
        displayCard(c3, "Queen of Hearts");

        PlayingCard c4 = new PlayingCard('C', 'X');
        displayCard(c4, "10 of Clubs");

        PlayingCard c5 = new PlayingCard('S', 'X');
        displayCard(c5, "10 of Spades");

        PlayingCard c6 = new PlayingCard('H', 'X');
        displayCard(c6, "10 of Hearts");

        PlayingCard c7 = new PlayingCard('D', 'X');
        displayCard(c7, "10 of Diamonds");

        PlayingCard c8 = new PlayingCard('C', 'X');
        displayCard(c8, "10 of Clubs");

        System.out.println("---------END OF DISPLAY!!!!!---------" + "\n");

        System.out.println("Equality Test Cases (equalTo())");
        System.out.println("--------------------------------");
        equality(c4, c1);
        equality(c4, c2);
        equality(c2, c4);
        equality(c2, c1);
        equality(c8, c4);
        equality(c4, c5);
        equality(c5, c5);
        System.out.print("\n");

        System.out.println("Comparison Test Cases (compareTo())");
        System.out.println("------------------------------------");
        System.out.println("Comparing Values");
        comparison(c1, c2);
        comparison(c2, c1);
        comparison(c2, c3);
        comparison(c4, c8);

        System.out.println("\n" + "Comparing Suits");
        comparison(c5, c6);
        comparison(c5, c7);
        comparison(c5, c8);
        System.out.println("");

        comparison(c6, c5);
        comparison(c6, c7);
        comparison(c6, c8);
        System.out.println("");

        comparison(c7, c5);
        comparison(c7, c6);
        comparison(c7, c8);
        System.out.println("");

        comparison(c8, c5);
        comparison(c8, c6);
        comparison(c8, c7);

    }
}