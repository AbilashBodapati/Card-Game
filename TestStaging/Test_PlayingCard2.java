
/**
 * TestPlayingCard2
 *
 * A simple test framework used to demonstrate the design of Java classes.
 * 
 * @author Jeremy Morris
 * @version 20120910
 */
public class Test_PlayingCard2 {

    /**
     * @param args
     */
    public static void main(String[] args) {

        PlayingCard pc1 = new PlayingCard('S', 'A');
        PlayingCard pc2 = new PlayingCard('H', 'A');
        PlayingCard pc3 = new PlayingCard('S', '2');
        PlayingCard pc4 = new PlayingCard('S', 'A');

        System.out.println(testEquals(pc1, pc2));
        System.out.println(testEquals(pc1, pc3));
        System.out.println(testEquals(pc1, pc4));

        System.out.println(testCompare(pc1, pc2));
        System.out.println(testCompare(pc2, pc1));
        System.out.println(testCompare(pc1, pc3));
        System.out.println(testCompare(pc1, pc4));

    }

    private static String testEquals(PlayingCard pc1, PlayingCard pc2) {
        String result = "";

        if (pc1.equals(pc2)) {
            result = result + " is the same as ";
        } else {
            result = result + " is not the same as ";
        }
        return pc1 + result + pc2;
    }

    private static String testCompare(PlayingCard pc1, PlayingCard pc2) {
        String result = "";
        if (pc1.compareTo(pc2) > 0) {
            result = result + " is better than ";
        } else if (pc1.compareTo(pc2) == 0) {
            result = result + " is the same as ";
        } else {
            result = result + " is worse than ";
        }
        return pc1 + result + pc2;
    }
}