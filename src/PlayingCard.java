/**
 * PlayingCard
 *
 * A simple class framework used to demonstrate the design of Java classes.
 *
 * @author Jeremy Morris
 * @version 20120910
 */

public class PlayingCard implements Comparable<PlayingCard> {

    // TODO - Setup private variables here
    private char suit;
    private char value;

    private String validSuits = "CDHS";
    private String validValues = "23456789XJQKA";

    public PlayingCard(char suit, char value) {
        // TODO - Implement constructor
        // This constructor should create the playing card
        // No Mutators in this class - once a card is created
        // its values never change
        this.suit = suit;
        this.value = value;
        if (!this.checkSuit()) {
            this.suit = 'X';
        }
        if (!this.checkValue()) {
            this.value = '0';
        }
    }

    public PlayingCard(int suit, int value) {

    }

    private boolean checkSuit() {
        int check = this.validSuits.indexOf(this.suit);
        boolean result;
        if (check >= 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
        // return (check>=0);
    }

    public boolean checkValue() {
        boolean result;
        int check = this.validValues.indexOf(this.value);
        if (check >= 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
        //return (check>=0 || Character.isDigit(this.value));
    }

    /*
     * getSuit() Returns the suit of the playing card as a character
     *
     * @return the suit of the playing card
     */
    public char getSuit() {
        // TODO - Implement getSuit()
        return this.suit;
    }

    /*
     * getValue() Returns the value of the playing card as a character
     *
     * @return value of the playing card
     */
    public char getValue() {
        // TODO - implement getValue()
        return this.value;
    }

    //	TODO - Implement the toString() method inherited from the
    //			Object class.  This should be implemented so that
    //			when a PlayingCard object is printed, it displays
    //			it's suit and value in the proper way.  For example,
    //			the 9 of Hearts should display as 9H while the Queen
    //			of Diamonds should display as QD.
//	@Override
    @Override
    public String toString() {
        String result = "";
        if (this.value == 'X') {
            result = result + "10";
        } else {
            result = result + this.value;
        }
        return result + this.suit;
    }

    //  TODO - SEE EXERCISE XX.  Do not try to implement this
    //		method until you reach EXECRISE XX
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (this.getClass() == obj.getClass()) {
            PlayingCard p1 = (PlayingCard) obj;
            if ((this.suit == p1.suit) && (this.value == p1.value)) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public int compareTo(PlayingCard arg0) {
        int result = 0;
        int value = 0;
        int argValue = 0;
        int suit = 0;
        int argSuit = 0;

        if (this.value == 'A') {
            value = 14;
        } else if (this.value == 'K') {
            value = 13;
        } else if (this.value == 'Q') {
            value = 12;
        } else if (this.value == 'J') {
            value = 11;
        } else if (this.value == 'X') {
            value = 10;
        } else {
            value = Character.getNumericValue(this.value);
        }

        if (arg0.value == 'A') {
            argValue = 14;
        } else if (arg0.value == 'K') {
            argValue = 13;
        } else if (arg0.value == 'Q') {
            argValue = 12;
        } else if (arg0.value == 'J') {
            argValue = 11;
        } else if (arg0.value == 'X') {
            argValue = 10;
        } else {
            argValue = Character.getNumericValue(arg0.value);
        }

        if (this.suit == 'S') {
            suit = 4;
        } else if (this.suit == 'H') {
            suit = 3;
        } else if (this.suit == 'D') {
            suit = 2;
        } else if (this.suit == 'C') {
            suit = 1;
        }

        if (arg0.suit == 'S') {
            argSuit = 4;
        } else if (arg0.suit == 'H') {
            argSuit = 3;
        } else if (arg0.suit == 'D') {
            argSuit = 2;
        } else if (arg0.suit == 'C') {
            argSuit = 1;
        }

        if (value < argValue) {
            result = -1;
        } else if (value > argValue) {
            result = 1;
        } else {
            if (suit < argSuit) {
                result = -1;
            } else if (suit > argSuit) {
                result = 1;
            } else {
                result = 0;
            }
        }
        return result;
    }

}