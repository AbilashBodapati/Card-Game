
/**
 * CardDeck
 *
 *   A simple class framework used to demonstrate the design
 *   of Java classes.
 *
 *   @author Jeremy Morris
 *   @version 20120910
 */
import java.util.ArrayList;
import java.util.Collections;

public class CardDeck {

    //TODO - Private variables here
    ArrayList<PlayingCard> deck;

    public CardDeck() {
        // TODO - implement the constructor
        // 		This constructor should create a new "empty"
        //		deck with no cards in it.  Just set up the
        //		initial variables.  This is useful for creating
        //		a "discard pile" for games that need it.
        this.deck = new ArrayList<PlayingCard>();
    }

    public void newDeck() {
        // TODO - implement this method.  It should create
        //	a new deck of 52 playing cards.  The deck should
        //	be a List of PlayingCard objects stored as a
        //  private member variable.  Initialize should take
        //  care to generate exactly the right cards with values
        //  2 through 10 and Ace, King, Queen and Jack for each
        //  of the four suits.
        //  NOTE:  Remember to take care of the case where
        //  you already have cards in your deck - you don't want
        //  to create duplicates!
        this.deck = new ArrayList<PlayingCard>();
        String values = "A23456789XJQK";
        String suits = "SDHC";
        for (int val = 0; val < values.length(); val++) {
            for (int suit = 0; suit < suits.length(); suit++) {
                char v = values.charAt(val);
                char s = suits.charAt(suit);
                PlayingCard pc = new PlayingCard(s, v);
                this.deck.add(pc);
            }
        }
    }

    public PlayingCard dealNext() {
        // TODO - implement this so that the "top" PlayingCard
        //	on your deck is removed and passed back to the calling
        //	program.  Note that you can decide which end of your
        //	List is the top card - you do not need to make it the
        //	card at position 0, though you may choose to.
        PlayingCard pc = null;
        if (this.deck.size() >= 1) {
            pc = this.deck.remove(0);
        }
        return pc;
    }

    public int remainingCards() {
        // TODO - returns the number of cards remaining in the
        //	deck.
        return this.deck.size();
    }

    // TODO - implement the toString() method to be something
    //	reasonable.  It is only going to be used for debugging
    //	purposes.  See Exercise ## for suggestions on what might
    //	be "reasonable".
//	@Override
    @Override
    public String toString() {
        return this.deck.toString();
    }

    public void shuffle() {
        // TODO - shuffle the deck so that the PlayingCards
        // 	stored in it are in a random order.  See the
        //	ClosedLab write-up for a hint about how to do this.

        Collections.shuffle(this.deck);
    }

}