/**
 * CardHand
 *
 * A simple class framework used to demonstrate the design of Java classes.
 *
 * @author Jeremy Morris
 * @SubAuthor Abilash Bodapati
 * @version 20190211
 */
public class CardHand {

    //TODO - private data members

    private PlayingCard hand;

    public CardHand(int maxSize) {
        // TODO - Write a constructor that sets up the private
        // member variables
        //  maxSize indicates the maximum number of cards that
        //  a hand is allowed to have

        this.hand = new PlayingCard(maxSize, maxSize);

    }

    public boolean add(PlayingCard pc) {
        // TODO - Fill in the method
        //  If there are fewer than the maximum number of cards
        //  in the hand, add this card and return true.
        //  Return false if you are at your maximum.
        return false;
    }

    public PlayingCard peekAt(int idx) {
        // TODO - Fill in the method
        //  Return the card at index idx if idx is within the
        //  maximum size of the hand.  Otherwise return null
        return null;
    }

    public PlayingCard discard(int idx) {
        // TODO - Fill in the method
        //  Remove the card at index idx if idx is within the
        //  maximum size of the hand.  Return that card.  If
        //  idx is too large or too small, return null
        return null;
    }

    public PlayingCard highestCard() {
        // TODO - Fill in the method
        //  Returns the card in the hand with the highest value

        return null;
    }

    @Override
    public String toString() {
        // TODO - Fill in the method
        //  This should return a string containing a list of the
        //  cards in the hand.  (HINT:  Look at what we did for
        //  CardDeck, which is similar)
        return "";
    }

    @Override
    public boolean equals(Object obj) {
        // TODO - Fill in the method
        // Two hands are equal if they contain the same cards
        boolean result = true;
        if (this.getClass() != obj.getClass()) {
            result = false;
        }
        return result;
    }

}